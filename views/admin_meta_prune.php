<div class="wrap" id="meta-prune-page">
	<h1 class="wp-heading-inline">Meta Prune</h1>
	<hr class="wp-header-end">

	<div id="ajax-response"></div>

	<?php if($success): ?>
	<div class="success notice">
	<p><?php echo htmlspecialchars($success); ?></p>
	</div>
	<?php endif; ?>

	<?php if($error): ?>
	<div class="error notice">
	<p><?php echo htmlspecialchars($error); ?></p>
	</div>
	<?php endif; ?>

	<p>Enter in a list of URLs separated by newlines. Each URL listed will receive a &lt;meta name="robots" content="noindex"&gt; tag.</p>

	<form action="admin-ajax.php" method="post">
		<?php wp_nonce_field( 'meta_prune_update_list' ); ?>
		<input name="plugin" type="hidden" value="meta_prune" />
		<input name="action" type="hidden" value="meta_prune_update_list" />
		<table class="form-table">
			<tr class="form-field form-required">
				<th scope="row"><label for="url">URL List</th>
				<td><textarea name="list"><?php echo htmlspecialchars($list); ?></textarea></td>
			</tr>
		</table>
		<p class="submit">
			<input type="submit" name="submit" id="submit" class="button button-primary" value="Update List">
		</p>
	</form>
</div>
