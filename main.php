<?php  
/* 
Plugin Name: Meta Prune
Plugin URI: http://agraddy.com
Description: Plugin that allows users to add meta noindex tags to pages
Author: Anthony Graddy
Version: 0.1.0 
*/  

class MetaPrune {
	public static $instance;
	public $db_version = 1;
	public $error = '';
	public $success = '';
	public $user;
	public $flash = array();
	public $message = '';
	public $plugin_key = 'meta_prune';

	function __construct() {
		define('META_PRUNE_BASE', dirname(__FILE__) . DIRECTORY_SEPARATOR);

		add_action('init', array($this, 'init'), 9);

		if(is_admin()) {
			register_activation_hook( __FILE__, array($this, 'install'));
		}

		MetaPrune::$instance = $this;
	}

	function addFlash($message, $type) {
		if(isset($this->flash[$type])) {
			$this->flash[$type] = $this->flash[$type] . ' ' . $message;
		} else {
			$this->flash[$type] = $message;
		}
	}

	function adminMenu() {
		global $current_user, $wp_roles, $wpdb;
		get_currentuserinfo();

		add_menu_page('Meta Prune', 'Meta Prune', 'read', 'meta-prune', array($this, 'viewMetaPrune'));
	}

	function head() {
		// https://stackoverflow.com/a/3997367
		$list = preg_split("/\\r\\n|\\r|\\n/", get_option($this->plugin_key . '_list'));

		// From: https://stackoverflow.com/a/6768831
		$url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

		if(in_array($url, $list)) {
			echo "\n";
			echo '<meta name="robots" content="noindex">';
			echo "\n";
		}
	}

	function viewMetaPrune() {
		$data = array();
		$data['list'] = get_option($this->plugin_key . '_list');
		$this->show('admin_meta_prune', $data);
	}

	function checkUpdateList() {
		if($_POST['plugin'] == $this->plugin_key && isset($_POST['list']) && wp_verify_nonce($_POST['_wpnonce'], 'meta_prune_update_list')) {
			$data = array();
			$data['list'] = $_POST['list'];
			$this->doUpdateList($data);
		} else {
			$output = array();
			$output['status'] = 'error';
			$output['message'] = 'There was a problem processing the request.';

			echo json_encode($output);
			die;
		}
	}

	function doUpdateList($data) {
		// Set up flash message
		$this->saveFlash('List has been updated.', 'success');

		// Update Option
		update_option($this->plugin_key . '_list', $data['list']);

		wp_safe_redirect( admin_url( 'admin.php?page=meta-prune' ) );
		die();
	}

	function adminAssets() {
		wp_enqueue_style($this->plugin_key . '_main', plugins_url('css/main.css', __FILE__));
		wp_enqueue_script($this->plugin_key . '_main', plugins_url('js/main.js', __FILE__), array('jquery'));
	}

	/*
	 * Sets up the plugin hooks other init items
	 */
	function init() {
		global $current_user, $wp_roles, $wpdb;
		get_currentuserinfo();

		add_action('admin_menu', array($this, 'adminMenu'));

		add_action('template_redirect', array($this, 'processFlash'), 5);
		add_action('admin_enqueue_scripts', array($this, 'adminAssets'));
		add_action('admin_notices', array($this, 'notifications'));

		add_action('wp_ajax_meta_prune_update_list', array($this, 'checkUpdateList')); 

		add_action('wp_head', array($this, 'head') );
	}

	// Install plugin
	function install() {
		global $wpdb, $current_user, $wp_roles;

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

		// Setup
		$migrations = maybe_unserialize(get_option($this->plugin_key . '_migrations'));
		if(!is_array($migrations)) {
			$migrations = array();
		}
		$notifications = array();

		// Migrations
		$mig = '2017-12-14';
		$notification = 'Initial setup.';
		if(!in_array($mig, $migrations)) {
			array_push($migrations, $mig);
			array_push($notifications, $notification);
		}

		// Cleanup
		update_option($this->plugin_key . '_migrations', serialize($migrations));
		update_option($this->plugin_key . '_notifications', serialize($notifications));
	}

	function notifications($attr) {
		$notifications = maybe_unserialize(get_option($this->plugin_key . '_notifications'));
		if(count($notifications)) {
			update_option($this->plugin_key . '_notifications', serialize(array()));
			$data = array();
			$data['message'] = 'The Meta Prune Plugin performed the following:';
			$data['notifications'] = $notifications;
			$this->show('notifications', $data);
		}
	}

	function processFlash() {
		if(isset($_COOKIE[$this->plugin_key . '_flash'])) {
			$temp = unserialize(base64_decode($_COOKIE[$this->plugin_key . '_flash']));

			$this->success = '';
			$this->error = '';
			foreach(array_keys($temp) as $key) {
				if($key == 'success') {
					$this->success .= $temp[$key];
				} else {
					$this->error .= $temp[$key];
				}
			}
		}
		if(count($this->flash)) {
			setcookie($this->plugin_key . '_flash', base64_encode(serialize($this->flash)), 0, '/');
		} else {
			// Remove cookie
			unset($_COOKIE[$this->plugin_key . '_flash']);
			setcookie($this->plugin_key . '_flash', '', time() - 3600, '/');
		}
	}

	// $type is usually success or error
	function saveFlash($message, $type) {
		$this->addFlash($message, $type);
		setcookie($this->plugin_key . '_flash', base64_encode(serialize($this->flash)), 0, '/');
	}

	/*
	 * Outputs view files. Variables passed in via $data become accessible in the view.
	 * For example, $data['user_id'] becomes $user_id in the view file.
	 */
	function show($view, $data = null) {
		// Extract the data variable so that 
		// $data['example'] can be called by
		// $example
		if(is_array($data)) {
			foreach($data as $key => $value) {
				${$key} = $value;
			}
		}
		$success = $this->success;
		$error = $this->error;

		include 'views' . DIRECTORY_SEPARATOR . $view . '.php';
	}


}

new MetaPrune();

